<html>
<head>
<style type="text/css">
.error {
    color:red;
    display:none;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">

function nombre_valido(valor) {
    var reg = /^[a-zA-Z \u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1]{0,45}$/;
    if (reg.test(valor)) return true;
    else return false;
}

$(document).ready(function () {
    $("#nombre").focusout(function () {
        if (!nombre_valido($(this).val())) $(".error.nombre").show();
        else $(".error.nombre").hide();
    });

});
</script>
</head>
<body>
<h2>Hello World!</h2>
<input name="nombre" val="" id="nombre">
    <br>
    <label class="error nombre">Ingrese un nombre v&aacute;lido</label><br>
    <input type="text">
</body>
</html>
