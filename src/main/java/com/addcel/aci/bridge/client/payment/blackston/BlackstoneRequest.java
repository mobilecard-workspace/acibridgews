package com.addcel.aci.bridge.client.payment.blackston;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlackstoneRequest {
	
	private Long idUsuario;
	private String concepto;
	private int idProveedor;
	private int idProducto;
	private double monto;
	private String cvv2;
	private String software;
	private String wkey;
	private String imei;
	private String tipo;
	private String modelo;
	private String zipCode;
	private String cardNumber;
	private String expDate;
    
	
	
	public BlackstoneRequest() {
		// TODO Auto-generated constructor stub
	}
	
	

	public String getCardNumber() {
		return cardNumber;
	}



	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}



	public String getExpDate() {
		return expDate;
	}



	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}



	public Long getIdUsuario() {
		return idUsuario;
	}


	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}


	public String getConcepto() {
		return concepto;
	}


	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}


	public int getIdProveedor() {
		return idProveedor;
	}


	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}


	public int getIdProducto() {
		return idProducto;
	}


	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}


	public double getMonto() {
		return monto;
	}


	public void setMonto(double monto) {
		this.monto = monto;
	}


	public String getCvv2() {
		return cvv2;
	}


	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}


	public String getSoftware() {
		return software;
	}


	public void setSoftware(String software) {
		this.software = software;
	}


	public String getWkey() {
		return wkey;
	}


	public void setWkey(String wkey) {
		this.wkey = wkey;
	}


	public String getImei() {
		return imei;
	}


	public void setImei(String imei) {
		this.imei = imei;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getModelo() {
		return modelo;
	}


	public void setModelo(String modelo) {
		this.modelo = modelo;
	}


	public String getZipCode() {
		return zipCode;
	}


	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	
	

}
