package com.addcel.aci.bridge.client.payment.business;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.aci.bridge.client.payment.vo.AcquirerSignon;
import com.addcel.aci.bridge.client.payment.vo.BankPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.CountryCode;
import com.addcel.aci.bridge.client.payment.vo.FinancialInstitution;
import com.addcel.aci.bridge.client.payment.vo.OfxBankPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.OfxBankPaymentResponse;
import com.addcel.aci.bridge.client.payment.vo.OfxCancelPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.OfxCancelPaymentResponse;
import com.addcel.aci.bridge.client.payment.vo.PMTINFO;
import com.addcel.aci.bridge.client.payment.vo.Payee;
import com.addcel.aci.bridge.client.payment.vo.Payer;
import com.addcel.aci.bridge.client.payment.vo.PayerNameType;
import com.addcel.aci.bridge.client.payment.vo.SignOnRequest;
import com.addcel.aci.bridge.client.payment.vo.BankPaymentRequest.PMTTRNRQ;
import com.addcel.aci.bridge.client.payment.vo.BankPaymentRequest.PMTTRNRQ.PMTRQ;
import com.addcel.aci.bridge.client.payment.vo.SignOnRequest.SONRQ;

public class Utils {

	private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);
	

	public static String getXMLRequest(OfxBankPaymentRequest BankRequest, String schema_location, Class... classesToBeBound){
		
		try{// https://collectpay-uat.princetonecom.com/pa/xml/createBankPayment2.do
	
			JAXBContext jaxbContext = JAXBContext.newInstance(classesToBeBound);
			 Marshaller m = jaxbContext.createMarshaller();
			// m.setProperty("xmlns", "http://www.princetonecom.com/pa/bankpaymentrequest2");
			 if(schema_location == null)
				 m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "https://collectpay-uat.princetonecom.com/pa/xml/schema/bank_payment_request_2.xsd");//"http://www.princetonecom.com/pa/bankpaymentrequest2 bank_payment_request_2.xsd");
			 else
				 m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, schema_location);
			 
			 StringWriter sw = new StringWriter();
			 m.marshal(BankRequest, sw);
			 return sw.toString();
		}catch(Exception ex){
			LOGGER.error("ERROR AL GENERAR XML REQUEST: " + ex.getMessage());
			ex.printStackTrace();
			return "";
		}
	}
	
	public static String getXMLRequestCancel(OfxCancelPaymentRequest BankRequest, String schema_location, Class... classesToBeBound){
		
		try{// https://collectpay-uat.princetonecom.com/pa/xml/createBankPayment2.do
	
			JAXBContext jaxbContext = JAXBContext.newInstance(classesToBeBound);
			 Marshaller m = jaxbContext.createMarshaller();
			 
			// m.setProperty("xmlns", "http://www.princetonecom.com/pa/bankpaymentrequest2");
			 if(schema_location == null)
				 m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "https://collectpay-uat.princetonecom.com/pa/xml/schema/cancel_payment_request_2.xsd");//"http://www.princetonecom.com/pa/bankpaymentrequest2 bank_payment_request_2.xsd");
			 else
				 m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, schema_location);
			 
			 StringWriter sw = new StringWriter();
			 m.marshal(BankRequest, sw);
			 return sw.toString();
		}catch(Exception ex){
			LOGGER.error("ERROR AL GENERAR XML REQUEST: " + ex.getMessage());
			ex.printStackTrace();
			return "";
		}
	}
	
	public static OfxBankPaymentResponse getObjectResponse(String response, Class... classesToBeBound ) {
		try{
			 JAXBContext jaxbContext = JAXBContext.newInstance(classesToBeBound);
			 Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			 InputStream stream = new ByteArrayInputStream(response.getBytes(StandardCharsets.UTF_8));
			 OfxBankPaymentResponse BankResponse = (OfxBankPaymentResponse) jaxbUnmarshaller.unmarshal(stream);
			 
			return BankResponse;
			
			}catch(Exception ex){
				LOGGER.error("ERROR AL OBTENER OBJECT REQUEST: " + ex.getMessage());
				ex.printStackTrace();
			    return null;	
			}
	}
	
	public static OfxCancelPaymentResponse getObjectResponseCancel(String response, Class... classesToBeBound ) {
		try{
			 JAXBContext jaxbContext = JAXBContext.newInstance(classesToBeBound);
			 Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			 InputStream stream = new ByteArrayInputStream(response.getBytes(StandardCharsets.UTF_8));
			 OfxCancelPaymentResponse BankResponse = (OfxCancelPaymentResponse) jaxbUnmarshaller.unmarshal(stream);
			 
			return BankResponse;
			
			}catch(Exception ex){
				LOGGER.error("ERROR AL OBTENER OBJECT REQUEST: " + ex.getMessage());
				ex.printStackTrace();
			    return null;	
			}
	}
	
}
