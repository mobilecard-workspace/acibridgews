//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.10.06 at 11:33:30 AM CDT 
//


package com.addcel.aci.bridge.client.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExpressCheck complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExpressCheck">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ORCC.CHECKTYPE" type="{http://www.princetonecom.com/pa/bankpaymentresponse2}CheckType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExpressCheck", propOrder = {
    "orccchecktype"
})
public class ExpressCheck {

    @XmlElement(name = "ORCC.CHECKTYPE", required = true)
    protected CheckType orccchecktype;

    /**
     * Gets the value of the orccchecktype property.
     * 
     * @return
     *     possible object is
     *     {@link CheckType }
     *     
     */
    public CheckType getORCCCHECKTYPE() {
        return orccchecktype;
    }

    /**
     * Sets the value of the orccchecktype property.
     * 
     * @param value
     *     allowed object is
     *     {@link CheckType }
     *     
     */
    public void setORCCCHECKTYPE(CheckType value) {
        this.orccchecktype = value;
    }

}
