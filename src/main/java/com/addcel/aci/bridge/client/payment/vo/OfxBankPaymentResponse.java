//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.10.06 at 11:33:30 AM CDT 
//


package com.addcel.aci.bridge.client.payment.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *  Response to create bank payment request. This schema follows the OFX naming conventions.
 *       
 * 
 * <p>Java class for OfxBankPaymentResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OfxBankPaymentResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SIGNONMSGSRSV1" type="{http://www.princetonecom.com/pa/bankpaymentresponse2}SignOnResponse"/>
 *         &lt;element name="BILLPAYMSGSRSV1" type="{http://www.princetonecom.com/pa/bankpaymentresponse2}BankPaymentResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement(name = "OFX")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfxBankPaymentResponse", propOrder = {
    "signonmsgsrsv1",
    "billpaymsgsrsv1"
})
public class OfxBankPaymentResponse {

    @XmlElement(name = "SIGNONMSGSRSV1", required = true)
    protected SignOnResponse signonmsgsrsv1;
    @XmlElement(name = "BILLPAYMSGSRSV1", required = true)
    protected BankPaymentResponse billpaymsgsrsv1;

    /**
     * Gets the value of the signonmsgsrsv1 property.
     * 
     * @return
     *     possible object is
     *     {@link SignOnResponse }
     *     
     */
    public SignOnResponse getSIGNONMSGSRSV1() {
        return signonmsgsrsv1;
    }

    /**
     * Sets the value of the signonmsgsrsv1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link SignOnResponse }
     *     
     */
    public void setSIGNONMSGSRSV1(SignOnResponse value) {
        this.signonmsgsrsv1 = value;
    }

    /**
     * Gets the value of the billpaymsgsrsv1 property.
     * 
     * @return
     *     possible object is
     *     {@link BankPaymentResponse }
     *     
     */
    public BankPaymentResponse getBILLPAYMSGSRSV1() {
        return billpaymsgsrsv1;
    }

    /**
     * Sets the value of the billpaymsgsrsv1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankPaymentResponse }
     *     
     */
    public void setBILLPAYMSGSRSV1(BankPaymentResponse value) {
        this.billpaymsgsrsv1 = value;
    }

}
