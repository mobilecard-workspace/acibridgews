package com.addcel.aci.bridge.client.payment.business;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.addcel.aci.bridge.bean.RequestFactoryBean;
import com.addcel.aci.bridge.client.payment.vo.AcquirerSignon;
import com.addcel.aci.bridge.client.payment.vo.BankPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.CountryCode;
import com.addcel.aci.bridge.client.payment.vo.FinancialInstitution;
import com.addcel.aci.bridge.client.payment.vo.OfxBankPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.OfxBankPaymentResponse;
import com.addcel.aci.bridge.client.payment.vo.PMTINFO;
import com.addcel.aci.bridge.client.payment.vo.Payee;
import com.addcel.aci.bridge.client.payment.vo.Payer;
import com.addcel.aci.bridge.client.payment.vo.PayerNameType;
import com.addcel.aci.bridge.client.payment.vo.SignOnRequest;
import com.addcel.aci.bridge.client.payment.vo.BankPaymentRequest.PMTTRNRQ;
import com.addcel.aci.bridge.client.payment.vo.BankPaymentRequest.PMTTRNRQ.PMTRQ;
import com.addcel.aci.bridge.client.payment.vo.SignOnRequest.SONRQ;


public class AciPayment {

	private static final Logger LOGGER = LoggerFactory.getLogger(AciPayment.class);
	private static final String endPoint = "https://collectpay-uat.princetonecom.com/pa/xml/createBankPayment2.do";
	
	 
	
	private OfxBankPaymentRequest BankRequest;
	private OfxBankPaymentResponse BankResponse;
	
	
	
	public OfxBankPaymentRequest getBankRequest() {
		return this.BankRequest;
	}

	public void setBankRequest(OfxBankPaymentRequest bankRequest) {
		this.BankRequest = bankRequest;
	}

	public OfxBankPaymentResponse getBankResponse() {
		return this.BankResponse;
	}

	public void setBankResponse(OfxBankPaymentResponse bankResponse) {
		this.BankResponse = bankResponse;
	}

	public void SendRequest(){
		try{
			RequestFactoryBean rfb = new RequestFactoryBean();
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			 //EJEMPLO CREAR REQUEST
			 
			map.add("xml",Utils.getXMLRequest(this.BankRequest,null,OfxBankPaymentRequest.class).replace("xmlns=\"http://www.princetonecom.com/pa/bankpaymentresponse2\"", "xmlns=\"http://www.princetonecom.com/pa/bankpaymentrequest2\"").replace("standalone=\"yes\"", ""));
			
			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

			ResponseEntity<String> response = restTemplate.postForEntity( endPoint, request , String.class );
			LOGGER.debug("Request: " + Utils.getXMLRequest(this.BankRequest,null,OfxBankPaymentRequest.class).replace("xmlns=\"http://www.princetonecom.com/pa/bankpaymentresponse2\"", "xmlns=\"http://www.princetonecom.com/pa/bankpaymentrequest2\"").replace("standalone=\"yes\"", ""));
			LOGGER.debug("Response ACI: " + response.getBody());
			
			this.BankResponse = Utils.getObjectResponse(response.getBody(), OfxBankPaymentResponse.class);
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL ENVIAR REQUEST: " + ex.getMessage());
		}
	}
	
	public void CreateRequest(){
		
		try{
			this.BankRequest = new OfxBankPaymentRequest();
			
			Date date = new Date();
			
			SignOnRequest sigOnRequest = new SignOnRequest();
			SONRQ sONRQ = new SONRQ();
			sONRQ.setDTCLIENT("20170131");  //date.getTime()+""
			AcquirerSignon acquirerSignon = new AcquirerSignon();
			acquirerSignon.setORCCACQUIRERID(570590);
			acquirerSignon.setORCCLOGINID("MobileCardUSAPAOConnectUser");
			acquirerSignon.setORCCPASSWORD("Hs2tpfU8jcnE");
			sONRQ.setORCCACQUIRERSIGNON(acquirerSignon);
			sONRQ.setLANGUAGE("EN");
			FinancialInstitution financialInstitution = new FinancialInstitution();
			financialInstitution.setORG("MOCUSA"); //MoblieCardUSA
			financialInstitution.setFID(570590); //  500170
			sONRQ.setFI(financialInstitution);
			sONRQ.setAPPID("PayAnyOne");
			sONRQ.setAPPVER(1);
			sigOnRequest.setSONRQ(sONRQ);
		
			
			BankPaymentRequest bankPaymentRequest = new BankPaymentRequest();
			PMTTRNRQ pMTTRNRQ = new PMTTRNRQ();
			pMTTRNRQ.setTRNUID("D11162016T143249957R305299314");
			PMTRQ pMTRQ = new PMTRQ();
			PMTINFO pMTINFO = new PMTINFO();
			pMTINFO.setTRNAMT(320);
			Payee payee =  new Payee();
			payee.setNAME("Direct TV");
			payee.setADDR1("123");
			payee.setADDR2("main st");
			payee.setCITY("Princeton");
			payee.setSTATE("NJ");
			payee.setPOSTALCODE("08540");
			
			payee.setCOUNTRY(CountryCode.US);
			pMTINFO.setPAYEE(payee);
			pMTINFO.setPAYACCT("x");
			pMTINFO.setDTDUE("20170131"); //date.getTime()+""
			
			Payer payer = new Payer();
			payer.setORCCPAYERID("x");
			PayerNameType payerNameType = new PayerNameType();
			payerNameType.setORCCFULLNAME("Test user");
			payer.setORCCPAYERNAME(payerNameType);
			payer.setADDR1("111");
			payer.setADDR2("xvz st");
			payer.setCITY("Princeton");
			payer.setSTATE("NJ");
			payer.setPOSTALCODE("08540");
			payer.setCOUNTRY(CountryCode.US);
			pMTINFO.setORCCPAYER(payer);
			pMTINFO.setORCCFIELD1("testFIELD1");
			pMTINFO.setORCCFIELD2("testFIELD2");
			
			pMTRQ.setPMTINFO(pMTINFO);
			pMTTRNRQ.setPMTRQ(pMTRQ);
			bankPaymentRequest.setPMTTRNRQ(pMTTRNRQ);
			
			this.BankRequest.setSIGNONMSGSRQV1(sigOnRequest);
			this.BankRequest.setBILLPAYMSGSRQV1(bankPaymentRequest);
			
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL CREAR REQUEST : " + ex.getLocalizedMessage());
		}
		
	}
	
	
}
