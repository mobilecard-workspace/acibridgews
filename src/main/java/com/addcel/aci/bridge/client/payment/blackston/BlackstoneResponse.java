package com.addcel.aci.bridge.client.payment.blackston;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlackstoneResponse {

		private int errorCode;
		private String message;
		private double amount;
		private long IdBitacora;
		private String Authorization;
		
		
		
		public BlackstoneResponse() {
			// TODO Auto-generated constructor stub
		}
		


		public long getIdBitacora() {
			return IdBitacora;
		}



		public void setIdBitacora(long idBitacora) {
			IdBitacora = idBitacora;
		}



		public String getAuthorization() {
			return Authorization;
		}



		public void setAuthorization(String authorization) {
			Authorization = authorization;
		}



		public int getErrorCode() {
			return errorCode;
		}
		public void setErrorCode(int errorCode) {
			this.errorCode = errorCode;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public double getAmount() {
			return amount;
		}
		public void setAmount(double amount) {
			this.amount = amount;
		}
	
	
}
