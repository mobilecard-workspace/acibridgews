package com.addcel.aci.bridge.mybatis.model.vo;

public class Class {
	
	private String clase_name;
	private String class_type;
	
	public Class() {
		// TODO Auto-generated constructor stub
	}
	
	public String getClase_name() {
		return clase_name;
	}
	public void setClase_name(String clase_name) {
		this.clase_name = clase_name;
	}
	public String getClass_type() {
		return class_type;
	}
	public void setClass_type(String class_type) {
		this.class_type = class_type;
	}
	
	
}
