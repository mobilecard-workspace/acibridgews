package com.addcel.aci.bridge.mybatis.model.vo;

public class Address {

	private String street;
	private String city;
	
	public Address() {
		// TODO Auto-generated constructor stub
	}
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
	
}
