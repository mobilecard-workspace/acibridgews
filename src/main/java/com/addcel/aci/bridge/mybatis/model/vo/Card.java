package com.addcel.aci.bridge.mybatis.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Card {
	
	private double	balance;
	private String	date; 
	private boolean	estado;
	private int	idTarjeta;
	private boolean	mobilecard; 
	private String	nombre;
	private String	numTarjeta; 
	private String	franquicia; 
	private String	vigencia;
	private String ct;
	private String domamex;
	private String cpamex;
	
	
	public Card() {
		// TODO Auto-generated constructor stub
	}


	public double getBalance() {
		return balance;
	}


	public void setBalance(double balance) {
		this.balance = balance;
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public boolean isEstado() {
		return estado;
	}


	public void setEstado(boolean estado) {
		this.estado = estado;
	}


	public int getIdTarjeta() {
		return idTarjeta;
	}


	public void setIdTarjeta(int idTarjeta) {
		this.idTarjeta = idTarjeta;
	}


	public boolean isMobilecard() {
		return mobilecard;
	}


	public void setMobilecard(boolean mobilecard) {
		this.mobilecard = mobilecard;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getNumTarjeta() {
		return numTarjeta;
	}


	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}


	public String getFranquicia() {
		return franquicia;
	}


	public void setFranquicia(String franquicia) {
		this.franquicia = franquicia;
	}


	public String getVigencia() {
		return vigencia;
	}


	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}


	public String getCt() {
		return ct;
	}


	public void setCt(String ct) {
		this.ct = ct;
	}


	public String getDomamex() {
		return domamex;
	}


	public void setDomamex(String domamex) {
		this.domamex = domamex;
	}


	public String getCpamex() {
		return cpamex;
	}


	public void setCpamex(String cpamex) {
		this.cpamex = cpamex;
	}
	
	
	

	
}
