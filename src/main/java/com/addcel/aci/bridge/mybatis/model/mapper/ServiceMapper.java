package com.addcel.aci.bridge.mybatis.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.aci.bridge.mybatis.model.vo.AciDetail;
import com.addcel.aci.bridge.mybatis.model.vo.Card;
import com.addcel.aci.bridge.mybatis.model.vo.ClienteVO;
import com.addcel.aci.bridge.mybatis.model.vo.ProjectMC;
import com.addcel.aci.bridge.mybatis.model.vo.Proveedor;
import com.addcel.aci.bridge.mybatis.model.vo.User;
import com.addcel.aci.bridge.mybatis.model.vo.servicios;




public interface ServiceMapper {

	public ClienteVO getClient(@Param(value = "user") String user, @Param(value = "pass") String pass);
	public List<servicios> getServicios(String estados);
	public int dataCheck(long id_usuario);
	public void DetailUpdate(AciDetail detail);
	public void DetailInsert(AciDetail detail);
	public int CvvCheck(@Param(value = "id_usuario") long id_usuario, @Param(value = "id_tarjeta") int id_tarjeta);
	public User getUserData(long id_usuario);
	public void updateUser(User user);
	public String getCt(@Param(value = "id_usuario") long id_usuario, @Param(value = "id_tarjeta") int id_tarjeta);
	public Proveedor getProveedor(String proveedor);
	public servicios getAddreses(int id_servicio);
	public Card getMobilecardCard(@Param(value = "id_usuario") long id_usuario, @Param(value = "id_tarjeta") int id_tarjeta);
	public String getEstado(@Param(value = "usr_id_estado")int usr_id_estado);
	public String getParameter(@Param(value = "parameter") String parameter);
	public ProjectMC getProjectMC( @Param(value = "cont") String cont,@Param(value = "recu") String recu);
}
