package com.addcel.aci.bridge.mybatis.model.vo;

public class AciDetail {

	private long id_bitacora;
	private int pmttrnrs_status_code;
	private String pmttrnrs_status_severity;
	private String pmttrnrs_status_message;
	private String pmtprcsts_pmtprccode;
	private String pmtprcsts_dtpmtprc;
	private String pmtprcsts_orccexpectedpmtdate;
	private long pmtprcsts_orccpaymentid;
	private double amount;
	
	private long id_service;
	private double comision;
	private String card_number;
	private long id_user;
	private double lat;
	private double lon;
	
	public AciDetail() {
		// TODO Auto-generated constructor stub
	}

	
	
	public long getId_service() {
		return id_service;
	}



	public void setId_service(long id_service) {
		this.id_service = id_service;
	}



	public double getComision() {
		return comision;
	}



	public void setComision(double comision) {
		this.comision = comision;
	}



	public String getCard_number() {
		return card_number;
	}



	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}



	public long getId_user() {
		return id_user;
	}



	public void setId_user(long id_user) {
		this.id_user = id_user;
	}



	public double getLat() {
		return lat;
	}



	public void setLat(double lat) {
		this.lat = lat;
	}



	public double getLon() {
		return lon;
	}



	public void setLon(double lon) {
		this.lon = lon;
	}



	public double getAmount() {
		return amount;
	}



	public void setAmount(double amount) {
		this.amount = amount;
	}



	public long getId_bitacora() {
		return id_bitacora;
	}

	public void setId_bitacora(long id_bitacora) {
		this.id_bitacora = id_bitacora;
	}

	public int getPmttrnrs_status_code() {
		return pmttrnrs_status_code;
	}

	public void setPmttrnrs_status_code(int pmttrnrs_status_code) {
		this.pmttrnrs_status_code = pmttrnrs_status_code;
	}

	public String getPmttrnrs_status_severity() {
		return pmttrnrs_status_severity;
	}

	public void setPmttrnrs_status_severity(String pmttrnrs_status_severity) {
		this.pmttrnrs_status_severity = pmttrnrs_status_severity;
	}

	public String getPmttrnrs_status_message() {
		return pmttrnrs_status_message;
	}

	public void setPmttrnrs_status_message(String pmttrnrs_status_message) {
		this.pmttrnrs_status_message = pmttrnrs_status_message;
	}

	public String getPmtprcsts_pmtprccode() {
		return pmtprcsts_pmtprccode;
	}

	public void setPmtprcsts_pmtprccode(String pmtprcsts_pmtprccode) {
		this.pmtprcsts_pmtprccode = pmtprcsts_pmtprccode;
	}

	public String getPmtprcsts_dtpmtprc() {
		return pmtprcsts_dtpmtprc;
	}

	public void setPmtprcsts_dtpmtprc(String pmtprcsts_dtpmtprc) {
		this.pmtprcsts_dtpmtprc = pmtprcsts_dtpmtprc;
	}

	public String getPmtprcsts_orccexpectedpmtdate() {
		return pmtprcsts_orccexpectedpmtdate;
	}

	public void setPmtprcsts_orccexpectedpmtdate(
			String pmtprcsts_orccexpectedpmtdate) {
		this.pmtprcsts_orccexpectedpmtdate = pmtprcsts_orccexpectedpmtdate;
	}

	public long getPmtprcsts_orccpaymentid() {
		return pmtprcsts_orccpaymentid;
	}

	public void setPmtprcsts_orccpaymentid(long pmtprcsts_orccpaymentid) {
		this.pmtprcsts_orccpaymentid = pmtprcsts_orccpaymentid;
	}
	
	
	
}
