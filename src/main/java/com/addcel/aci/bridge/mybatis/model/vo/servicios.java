package com.addcel.aci.bridge.mybatis.model.vo;

import java.lang.reflect.Type;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class servicios {
	//addressJson _classJson
	private long id;
	private String name;
	private List<Class> _class;
	private List<Address> address;
	private String addressJson;
	private String _classJson;
	private double pctComision;

	Gson gson= new Gson();
	
	public servicios() {
		// TODO Auto-generated constructor stub
	}
	
	

	public double getPctComision() {
		return pctComision;
	}



	public void setPctComision(double pctComision) {
		this.pctComision = pctComision;
	}



	public List<Class> get_class() {
		return _class;
	}


	public void set_class(List<Class> _class) {
		this._class = _class;
	}


	public List<Address> getAddress() {
		return address;
	}


	public void setAddress(List<Address> address) {
		this.address = address;
	}


	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	

	public String getAddressJson() {
		return addressJson;
	}



	public void setAddressJson(String addressJson) {
		this.addressJson = addressJson.substring(0,addressJson.length()-1).replace("{ \"address\":", "");
		JsonParser  parser = new JsonParser();
        JsonElement elem   = parser.parse(this.addressJson);

         com.google.gson.JsonArray elemArr = elem.getAsJsonArray();
         Type collectionType = new TypeToken<List<Address>>(){}.getType();
         List<Address> c = (List<Address>) gson.fromJson(elem, collectionType);
         this.address = c;
         this.addressJson = null;
	}



	public String get_classJson() {
		return _classJson;
	}



	public void set_classJson(String _classJson) {
		this._classJson = _classJson.substring(0,_classJson.length()-1).replace("{\"class\":", "");;
		/*JsonParser  parser = new JsonParser();
        JsonElement elem   = parser.parse(this._classJson);

         com.google.gson.JsonArray elemArr = elem.getAsJsonArray();
         Type collectionType = new TypeToken<List<Class>>(){}.getType();
         List<Class> c = (List<Class>) gson.fromJson(elem, collectionType);
         this._class = c;*/
         this._classJson = null;
		
	}


	
	
}
