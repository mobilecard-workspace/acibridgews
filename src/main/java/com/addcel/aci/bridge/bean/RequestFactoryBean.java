package com.addcel.aci.bridge.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

public class RequestFactoryBean implements FactoryBean<ClientHttpRequestFactory> {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequestFactoryBean.class);
	
	    private String keyStoreFileName;

	    private char[] keyStorePassword;

	    @Override
	    public ClientHttpRequestFactory getObject() throws Exception {

	        // (1)
	    	 /*java.lang.System.setProperty(
	    	          "jdk.tls.client.protocols", "TLSv1,TLSv1.1,TLSv1.2");*/
	        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
	        File origen = new File("/home/rhtrejo/jdk1.8.0_65/jre/lib/security/cacerts");
	        InputStream in = new FileInputStream(origen);
	        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
	        //this.getClass().getClassLoader().getResourceAsStream(this.keyStoreFileName)
	        ks.load(in,
	                "changeit".toCharArray());

	        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory
	                .getDefaultAlgorithm());
	        kmf.init(ks, "mobilecard".toCharArray());

	        System.out.println("tamaño de keystore: " + ks.size());
	        TrustManagerFactory tmf = TrustManagerFactory
	                .getInstance(TrustManagerFactory.getDefaultAlgorithm());
	        tmf.init(ks);
	        
	        sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

	        // (2)
	        HttpClient httpClient = HttpClientBuilder.create().setSslcontext(sslContext).build();

	        // (3)
	        ClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(
	                httpClient);
	        System.out.println("***********************************INICIANDO SSL****************************************");
	        System.out.println("***********************************INICIANDO SSL****************************************");
	        System.out.println("***********************************INICIANDO SSL****************************************");
	        System.out.println("***********************************INICIANDO SSL****************************************");
	        System.out.println("***********************************INICIANDO SSL****************************************");
	        in.close();
	        return factory;
	    }

	    @Override
	    public Class<?> getObjectType() {
	        return ClientHttpRequestFactory.class;
	    }

	    @Override
	    public boolean isSingleton() {
	        return true;
	    }

	    public void setKeyStoreFileName(String keyStoreFileName) {
	        this.keyStoreFileName = keyStoreFileName;
	    }

	    public void setKeyStorePassword(char[] keyStorePassword) {
	        this.keyStorePassword = keyStorePassword;
	    }
}
