package com.addcel.aci.bridge.spring.model;

import java.util.List;

import com.addcel.aci.bridge.mybatis.model.vo.Address;

public class AddressResponse {
	
	private List<Address> address;
	
	public AddressResponse() {
		// TODO Auto-generated constructor stub
	}

	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}
	
	

}
