package com.addcel.aci.bridge.spring.model;

public class CheckUserDataRequest {
	
	private long idUsuario;
	private int idTarjeta;
	
	public CheckUserDataRequest() {
		// TODO Auto-generated constructor stub
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdTarjeta() {
		return idTarjeta;
	}

	public void setIdTarjeta(int idTarjeta) {
		this.idTarjeta = idTarjeta;
	}

	
	
	
}
