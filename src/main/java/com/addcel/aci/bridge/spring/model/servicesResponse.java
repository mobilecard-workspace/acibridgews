package com.addcel.aci.bridge.spring.model;

import java.util.List;

import com.addcel.aci.bridge.mybatis.model.vo.Address;
import com.addcel.aci.bridge.mybatis.model.vo.Class;

public class servicesResponse{
	
	private long id;
	private String name;
	private List<Class> _class;
	private List<Address> address;
	
	public servicesResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Class> get_class() {
		return _class;
	}
	public void set_class(List<Class> _class) {
		this._class = _class;
	}
	public List<Address> getAddress() {
		return address;
	}
	public void setAddress(List<Address> address) {
		this.address = address;
	}
	
	

}
