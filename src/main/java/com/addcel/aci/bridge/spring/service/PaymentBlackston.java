package com.addcel.aci.bridge.spring.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.addcel.aci.bridge.client.payment.blackston.BlackstoneRequest;
import com.addcel.aci.bridge.client.payment.blackston.BlackstoneResponse;
import com.google.gson.Gson;




@Service
public class PaymentBlackston {

	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentBlackston.class);
	private static final String endPointBlackstone = "http://localhost:80/MCBlackstoneServices/mobilecard/blackstone/send/payment";
	//PROD 192.168.75.51:8081
	//QA 192.168.75.53:8089
	
	public BlackstoneResponse BlackstonePay(BlackstoneRequest Brequest){
		
		try{
			Gson gson = new Gson();		
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("json", gson.toJson(Brequest));
			LOGGER.debug("Request Blackston: " +  gson.toJson(Brequest));
			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<BlackstoneResponse> response = restTemplate.postForEntity(endPointBlackstone, map,BlackstoneResponse.class);
			
			LOGGER.debug("Respuesta Blackston :" + gson.toJson(response.getBody()));

			return response.getBody();
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
}
