package com.addcel.aci.bridge.spring.service;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonProperty;

//@Service
public class RequestPay {

	private String idTransaction; //Transaction Code. The client’s identifier for the request. 45 Alphanumeric
	
	@JsonProperty("TRNAMT")
	private double TRNAMT; //Payment amount. 12,2 Decimal
	
	@JsonProperty("NAME")
	private String NAME; //The biller’s name. 45 AllChars
	
	@JsonProperty("ADDR1")
	private String ADDR1; //Biller’s address line1. 64 AllChars
	
	@JsonProperty("CITY")
	private String CITY; //Biller’s city. 32 AllChars
	
	@JsonProperty("STATE")
	private String STATE; //Biller’s state. 32 AllChars
	
	@JsonProperty("POSTALCODE")
	private String POSTALCODE; //Biller’s postal code. 9 Alphanumeric
	
	@JsonProperty("PAYACCT")
	private String PAYACCT; // Original Billing account number. 60 Alphanumeric
	
	@JsonProperty("ORCCPAYERID")
	private String ORCCPAYERID; //id del usuario(preguntar bien)
	
	@JsonProperty("ORCCFULLNAME")
	private String ORCCFULLNAME; // user name
	
	@JsonProperty("ORCCADDR1")
	private String ORCCADDR1; // user address
	
	@JsonProperty("ORCCCITY")
	private String ORCCCITY; // user city
	
	@JsonProperty("ORCCSTATE")
	private String ORCCSTATE; // user state
	
	@JsonProperty("ORCCCP")
	private String ORCCCP; //user postal code
	
	public RequestPay() {
		// TODO Auto-generated constructor stub
	}

	public String getIdTransaction() {
		return idTransaction;
	}

	public void setIdTransaction(String idTransaction) {
		this.idTransaction = idTransaction;
	}

	public double getTRNAMT() {
		return TRNAMT;
	}

	public void setTRNAMT(double tRNAMT) {
		TRNAMT = tRNAMT;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getADDR1() {
		return ADDR1;
	}

	public void setADDR1(String ADDR1) {
		this.ADDR1 = ADDR1;
	}

	public String getCITY() {
		return CITY;
	}

	public void setCITY(String CITY) {
		this.CITY = CITY;
	}

	public String getSTATE() {
		return STATE;
	}

	public void setSTATE(String STATE) {
		this.STATE = STATE;
	}

	public String getPOSTALCODE() {
		return POSTALCODE;
	}

	public void setPOSTALCODE(String POSTALCODE) {
		this.POSTALCODE = POSTALCODE;
	}

	public String getPAYACCT() {
		return PAYACCT;
	}

	public void setPAYACCT(String pAYACCT) {
		PAYACCT = pAYACCT;
	}

	public String getORCCPAYERID() {
		return ORCCPAYERID;
	}

	public void setORCCPAYERID(String oRCCPAYERID) {
		ORCCPAYERID = oRCCPAYERID;
	}

	public String getORCCFULLNAME() {
		return ORCCFULLNAME;
	}

	public void setORCCFULLNAME(String oRCCFULLNAME) {
		ORCCFULLNAME = oRCCFULLNAME;
	}

	public String getORCCADDR1() {
		return ORCCADDR1;
	}

	public void setORCCADDR1(String oRCCADDR1) {
		ORCCADDR1 = oRCCADDR1;
	}

	public String getORCCCITY() {
		return ORCCCITY;
	}

	public void setORCCCITY(String oRCCCITY) {
		ORCCCITY = oRCCCITY;
	}

	public String getORCCSTATE() {
		return ORCCSTATE;
	}

	public void setORCCSTATE(String oRCCSTATE) {
		ORCCSTATE = oRCCSTATE;
	}

	public String getORCCCP() {
		return ORCCCP;
	}

	public void setORCCCP(String oRCCCP) {
		ORCCCP = oRCCCP;
	}
	
	
	
}
