package com.addcel.aci.bridge.spring.controller;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import com.addcel.aci.bridge.bean.RequestFactoryBean;
import com.addcel.aci.bridge.client.payment.blackston.BlackstoneRequest;
import com.addcel.aci.bridge.client.payment.blackston.BlackstoneResponse;
import com.addcel.aci.bridge.client.payment.business.AciPayment;
import com.addcel.aci.bridge.client.payment.business.Utils;
import com.addcel.aci.bridge.client.payment.vo.AcquirerSignon;
import com.addcel.aci.bridge.client.payment.vo.BankPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.BankPaymentRequest.PMTTRNRQ;
import com.addcel.aci.bridge.client.payment.vo.BankPaymentRequest.PMTTRNRQ.PMTRQ;
import com.addcel.aci.bridge.client.payment.vo.BankPaymentResponse;
import com.addcel.aci.bridge.client.payment.vo.CancelPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.CountryCode;
import com.addcel.aci.bridge.client.payment.vo.FinancialInstitution;
import com.addcel.aci.bridge.client.payment.vo.OfxBankPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.OfxBankPaymentResponse;
import com.addcel.aci.bridge.client.payment.vo.OfxCancelPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.OfxCancelPaymentResponse;
import com.addcel.aci.bridge.client.payment.vo.PMTINFO;
import com.addcel.aci.bridge.client.payment.vo.Payee;
import com.addcel.aci.bridge.client.payment.vo.Payer;
import com.addcel.aci.bridge.client.payment.vo.PayerNameType;
import com.addcel.aci.bridge.client.payment.vo.SignOnRequest;
import com.addcel.aci.bridge.client.payment.vo.SignOnRequest.SONRQ;
import com.addcel.aci.bridge.mybatis.model.mapper.ServiceMapper;
import com.addcel.aci.bridge.mybatis.model.vo.Address;
import com.addcel.aci.bridge.mybatis.model.vo.ClienteVO;
import com.addcel.aci.bridge.mybatis.model.vo.Proveedor;
import com.addcel.aci.bridge.mybatis.model.vo.User;
import com.addcel.aci.bridge.mybatis.model.vo.servicios;
import com.addcel.aci.bridge.mybatis.model.vo.Class;
import com.addcel.aci.bridge.spring.model.AddressResponse;
import com.addcel.aci.bridge.spring.model.CheckUserDataRequest;
import com.addcel.aci.bridge.spring.model.McResponse;
import com.addcel.aci.bridge.spring.model.PayRequest;
import com.addcel.aci.bridge.spring.model.jsonClass;
import com.addcel.aci.bridge.spring.model.servicesResponse;
import com.addcel.aci.bridge.spring.service.PaymentACI;
import com.addcel.aci.bridge.spring.service.PaymentBlackston;
import com.addcel.aci.bridge.spring.service.RequestPay;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.util.function.Consumer;
import java.lang.reflect.Type;

@RestController
public class RestServices {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestServices.class);
	
	
	@Autowired
	private PaymentACI requestPay;
	
	@RequestMapping(value = "/getServices", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public List<servicios> getServices(){
		//requestPay.getRestemplate();
		return requestPay.getServices();
	}
	
	@RequestMapping(value = "/{id_service}/addresses", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public List<Address>  getAddresses(@PathVariable int id_service){
		
		return requestPay.getAddresses(id_service);
	
	}
	
	@RequestMapping(value = "/CheckUserData", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public McResponse CheckData(@RequestBody CheckUserDataRequest request){
	
		return requestPay.CheckData(request);
	}
	
	//"/payment"
	
	@RequestMapping(value = "/payment", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public McResponse payment(@RequestBody PayRequest request){
		
		return requestPay.payment(request);
	}
	
	@RequestMapping(value = "/paymentNotData", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	public McResponse paymentNotData(@RequestBody PayRequest request){
		
		return requestPay.paymentNotData(request);
	}
	
	@RequestMapping(value = "/AciService", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public jsonClass getEmployeeInJSON() {
		jsonClass jc = new jsonClass();
		jc.setFirstName("ruben");
		jc.setId(1000l);
		jc.setLastName("Quiroz");
		AciPayment aci = new AciPayment();
		aci.CreateRequest();
		//aci.setBankRequest(Utils.CreateRequest(aci.getBankRequest()));
		aci.SendRequest();
		System.out.println(aci.getBankResponse().getSIGNONMSGSRSV1().getSONRS().getSTATUS().getMESSAGE());
		//ClienteVO c = mapper.getClient("SCT", "5EcC0m7RaSkCThy3");
		
		
		
		
		//System.out.println(c.getAfiliacion() + "---------*************************************************************************");
		//System.out.println(c.getAlias_organizacion() + "---------*************************************************************************");
   	 return jc;
 
    }
	
	@RequestMapping(value = "/cancel", method = RequestMethod.GET, produces = "application/json")
	public OfxCancelPaymentResponse cancel(){
		
		return requestPay.cancel();
	}
	
	@GetMapping("/jsonClass")
	public ResponseEntity getCustomers() {
		
		jsonClass jc = new jsonClass();
		URI uri= null;
		try{
		
	
		/*RequestFactoryBean rfb = new RequestFactoryBean();
		RestTemplate restTemplate = new RestTemplate();
		
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("xml", "<xml></xml>");
       System.out.println("INICIANDO CONEXION SSL CON ACI ----****************************************");
       //  uri = restTemplate.postForLocation("https://collectpay-uat.princetonecom.com/pa/xml/createBankPayment2.do", map);
       String result = restTemplate.getForObject( "https://collectpay-uat.princetonecom.com/pa/xml/createBankPayment2.do", String.class); 
       System.out.println("Location : "+result);*/
    
			enviapost();
		
		jc.setFirstName("ruben");
		jc.setId(1000l);
		jc.setLastName("Quiroz");
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return new ResponseEntity(jc, HttpStatus.OK);
	}
	
	private void  enviapost(){
		try{
			RequestFactoryBean rfb = new RequestFactoryBean();
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			 //EJEMPLO CREAR REQUEST
			 
			map.add("xml",CreateRequest());
			

			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

			ResponseEntity<String> response = restTemplate.postForEntity( "https://collectpay-uat.princetonecom.com/pa/xml/createBankPayment2.do", request , String.class );
			System.out.println("BODY----->>: " + response.getBody());
			
			//serialiar el xml de respuesta a un objeto*****
			FromXMLtoObject(response.getBody());
			
		}catch(Exception ex){
			System.out.print("Error al enviar peticion");
			ex.printStackTrace();
		}
	}
	
	
	
	private String CreateRequest(){
		String request="";
		OfxBankPaymentRequest Ofxrequest = new OfxBankPaymentRequest();
		
		SignOnRequest sigOnRequest = new SignOnRequest();
		SONRQ sONRQ = new SONRQ();
		sONRQ.setDTCLIENT("20090901");
		AcquirerSignon acquirerSignon = new AcquirerSignon();
		acquirerSignon.setORCCLOGINID("500170");
		acquirerSignon.setORCCLOGINID("adminorcc");
		acquirerSignon.setORCCPASSWORD("pass2222");
		sONRQ.setORCCACQUIRERSIGNON(acquirerSignon);
		sONRQ.setLANGUAGE("");
		FinancialInstitution financialInstitution = new FinancialInstitution();
		financialInstitution.setORG("TEST FI NAME");
		financialInstitution.setFID(500170);
		sONRQ.setFI(financialInstitution);
		sONRQ.setAPPID("PayAnyOne");
		sONRQ.setAPPVER(1);
		sigOnRequest.setSONRQ(sONRQ);
		
		
		BankPaymentRequest bankPaymentRequest = new BankPaymentRequest();
		PMTTRNRQ pMTTRNRQ = new PMTTRNRQ();
		PMTRQ pMTRQ = new PMTRQ();
		PMTINFO pMTINFO = new PMTINFO();
		pMTINFO.setTRNAMT(320);
		Payee payee =  new Payee();
		payee.setNAME("Direct TV");
		payee.setADDR1("123");
		payee.setADDR2("main st");
		payee.setCITY("Princeton");
		payee.setSTATE("NJ");
		payee.setPOSTALCODE("08540");
		
		payee.setCOUNTRY(CountryCode.US);
		pMTINFO.setPAYEE(payee);
		pMTINFO.setPAYACCT("x");
		pMTINFO.setDTDUE("20090901");
		
		Payer payer = new Payer();
		payer.setORCCPAYERID("x");
		PayerNameType payerNameType = new PayerNameType();
		payerNameType.setORCCFULLNAME("Test user");
		payer.setORCCPAYERNAME(payerNameType);
		payer.setADDR1("111");
		payer.setADDR2("xvz st");
		payer.setCITY("Princeton");
		payer.setSTATE("NJ");
		payer.setPOSTALCODE("08540");
		payer.setCOUNTRY(CountryCode.US);
		pMTINFO.setORCCPAYER(payer);
		pMTINFO.setORCCFIELD1("testFIELD1");
		pMTINFO.setORCCFIELD2("testFIELD2");
		
		pMTRQ.setPMTINFO(pMTINFO);
		pMTTRNRQ.setPMTRQ(pMTRQ);
		bankPaymentRequest.setPMTTRNRQ(pMTTRNRQ);
		
		Ofxrequest.setSIGNONMSGSRQV1(sigOnRequest);
		Ofxrequest.setBILLPAYMSGSRQV1(bankPaymentRequest);
		
		try{
			System.out.println("\n\n****************REQUEST CREADO*************************");
			JAXBContext jaxbContext = JAXBContext.newInstance(OfxBankPaymentRequest.class);
			 Marshaller m = jaxbContext.createMarshaller();
			 m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.princetonecom.com/pa/bankpaymentrequest2 bank_payment_request_2.xsd");
			 
			 StringWriter sw = new StringWriter();
			 m.marshal(Ofxrequest, sw);
			 System.out.println(sw.toString());
		}catch(Exception ex){
			System.out.println("Error al crear request...");
			ex.printStackTrace();
		}
		
		
		return request;
	}

	private void FromXMLtoObject(String response){
		try{
		 JAXBContext jaxbContext = JAXBContext.newInstance(OfxBankPaymentResponse.class);
		 Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		 InputStream stream = new ByteArrayInputStream(response.getBytes(StandardCharsets.UTF_8));
		 OfxBankPaymentResponse BankResponse = (OfxBankPaymentResponse) jaxbUnmarshaller.unmarshal(stream);
		 System.out.println("Obejct --------> " + BankResponse.getSIGNONMSGSRSV1().getSONRS().getSTATUS().getCODE());
		 
		
		}catch(Exception ex){
			System.out.println("Error al seralizar xml a objecto");
			ex.printStackTrace();
		}
	}
	
	
	@PostMapping(value = "/jsonClass")
	public ResponseEntity createCustomer() {

		jsonClass jc = new jsonClass();
		jc.setFirstName("ruben");
		jc.setId(1000l);
		jc.setLastName("Quiroz");

		return new ResponseEntity(jc, HttpStatus.OK);
	}


}
