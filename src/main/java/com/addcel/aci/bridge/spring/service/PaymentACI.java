package com.addcel.aci.bridge.spring.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.SSLContext;

import org.apache.commons.httpclient.HttpClient;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.addcel.aci.bridge.bean.RequestFactoryBean;
import com.addcel.aci.bridge.client.payment.blackston.BlackstoneRequest;
import com.addcel.aci.bridge.client.payment.blackston.BlackstoneResponse;
import com.addcel.aci.bridge.client.payment.business.Utils;
import com.addcel.aci.bridge.client.payment.vo.AcquirerSignon;
import com.addcel.aci.bridge.client.payment.vo.BankPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.CancelPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.CountryCode;
import com.addcel.aci.bridge.client.payment.vo.FinancialInstitution;
import com.addcel.aci.bridge.client.payment.vo.OfxBankPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.OfxBankPaymentResponse;
import com.addcel.aci.bridge.client.payment.vo.OfxCancelPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.OfxCancelPaymentResponse;
import com.addcel.aci.bridge.client.payment.vo.PMTINFO;
import com.addcel.aci.bridge.client.payment.vo.Payee;
import com.addcel.aci.bridge.client.payment.vo.Payer;
import com.addcel.aci.bridge.client.payment.vo.PayerNameType;
import com.addcel.aci.bridge.client.payment.vo.SignOnRequest;
import com.addcel.aci.bridge.client.payment.vo.BankPaymentRequest.PMTTRNRQ;
import com.addcel.aci.bridge.client.payment.vo.BankPaymentRequest.PMTTRNRQ.PMTRQ;
import com.addcel.aci.bridge.client.payment.vo.SignOnRequest.SONRQ;
import com.addcel.aci.bridge.mybatis.model.mapper.ServiceMapper;
import com.addcel.aci.bridge.mybatis.model.vo.AciDetail;
import com.addcel.aci.bridge.mybatis.model.vo.Address;
import com.addcel.aci.bridge.mybatis.model.vo.Card;
import com.addcel.aci.bridge.mybatis.model.vo.ProjectMC;
import com.addcel.aci.bridge.mybatis.model.vo.Proveedor;
import com.addcel.aci.bridge.mybatis.model.vo.User;
import com.addcel.aci.bridge.mybatis.model.vo.servicios;
import com.addcel.aci.bridge.spring.model.AddressResponse;
import com.addcel.aci.bridge.spring.model.CheckUserDataRequest;
import com.addcel.aci.bridge.spring.model.McResponse;
import com.addcel.aci.bridge.spring.model.PayRequest;
import com.addcel.aci.bridge.utils.Constants;
import com.addcel.aci.bridge.utils.UtilsGeneric;
import com.addcel.aci.bridge.utils.UtilsMC;
import com.addcel.utils.AddcelCrypto;
import com.sun.org.apache.bcel.internal.generic.IDIV;

@Service
public class PaymentACI {
	
	 public static final X509HostnameVerifier ALLOW_ALL_HOSTNAME_VERIFIER     = new AllowAllHostnameVerifier();
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentACI.class);
	private static final String endPoint_Pay = "https://collectpay.princetonecom.com/pa/xml/createBankPayment2.do";
	//PROD https://collectpay.princetonecom.com/pa/xml/createBankPayment2.do
	// QA https://collectpay-uat.princetonecom.com/pa/xml/createBankPayment2.do
	
	private static final String endPoint_cancel = "https://collectpay.princetonecom.com/pa/xml/cancelPayment2.do";
	//PROD https://collectpay.princetonecom.com/pa/xml/cancelPayment2.do
	// QA https://collectpay-uat.princetonecom.com/pa/xml/cancelPayment2.do
	
	@Autowired
	private ServiceMapper mapper;
	
	@Autowired
	private PaymentBlackston paymentBlacks;
	
	//@Autowired 
	//private RestTemplate restTemplate;
	
	
	public McResponse CheckData(CheckUserDataRequest request){

		McResponse response = new McResponse();
		response.setIdError(100);
		response.setMensajeError("The user does not have the necessary data");
		try{
		int error = mapper.dataCheck(request.getIdUsuario());
		if(error == 1){
			
			error = mapper.CvvCheck(request.getIdUsuario(), request.getIdTarjeta());
			if(error == 1){
				response.setIdError(0);
				response.setMensajeError("");
			}
		}
		
		}catch(Exception ex){
			LOGGER.error("Error: ", ex);
		}
		return response;
	}
	
	public List<Address>  getAddresses (int id_service){
		try{
			servicios services = mapper.getAddreses(id_service);
			
			AddressResponse  address = new AddressResponse();
			address.setAddress(services.getAddress());
			
			return address.getAddress();
		}catch(Exception  ex){
			return new ArrayList<Address>();
		}
	}
	
	public  List<servicios> getServices(){
		
		try{
			List<servicios> services = mapper.getServicios(""); 
			return services;
		}catch(Exception ex){
			return new ArrayList<servicios>();
		}
	}

	public McResponse payment(PayRequest request){

			LOGGER.debug("Iniciando Proceso de Pago con datos usuario: ");
			
			NumberFormat currformat = NumberFormat.getCurrencyInstance(Locale.US);
			
			
			McResponse Mc =  new McResponse();
			SimpleDateFormat DFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		
		try{	
			User user =  mapper.getUserData(request.getIdUsuario());
			//String ct = mapper.getCt(request.getIdUsuario(), request.getIdTarjeta());
			Card card = mapper.getMobilecardCard(request.getIdUsuario(), request.getIdTarjeta());
		   BlackstoneRequest Brequest = new BlackstoneRequest();
		   Proveedor proveedor= mapper.getProveedor("ACI");
		   LOGGER.debug("Después de buscar proveedor" + proveedor.getId_proveedor());
		   
		   double comision =  (request.getMonto()*proveedor.getComision_porcentaje());
		   double Mtotal = request.getMonto() + comision;//(request.getMonto()*proveedor.getComision_porcentaje());
		   
		   
		  
		   Brequest.setConcepto("PAGO ACI: " + request.getNombreServicio());
		   Brequest.setCvv2( UtilsService.getSMS(card.getCt()) );
		   Brequest.setCardNumber(UtilsService.getSMS(card.getNumTarjeta()));
		   Brequest.setExpDate(UtilsService.getSMS(card.getVigencia()).replace("/", ""));
		   Brequest.setIdProducto(request.getIdServicio());
		   Brequest.setIdProveedor(proveedor.getId_proveedor().intValue());
		   Brequest.setIdUsuario(request.getIdUsuario());
		   Brequest.setImei(request.getImei());
		   Brequest.setModelo(request.getModelo());
		   Brequest.setMonto(Mtotal);
		   Brequest.setSoftware(request.getSoftware());
		   Brequest.setTipo("M");
		   Brequest.setWkey(request.getWkey());
		   Brequest.setZipCode(user.getUsr_cp());
		   LOGGER.debug("Enviando peticion a Blackston");
		   BlackstoneResponse Bresp = paymentBlacks.BlackstonePay(Brequest);
		   
		   //guardar en bitacora aci detalle
		   AciDetail details = new AciDetail();
		   details.setId_bitacora(Bresp.getIdBitacora());
		   details.setId_service(request.getIdServicio());
		   details.setComision(comision);
		   details.setCard_number(card.getNumTarjeta());
		   details.setId_user(request.getIdUsuario());
		   details.setLat(request.getLat());
		   details.setLon(request.getLon());
		   details.setAmount(Mtotal);
		   mapper.DetailInsert(details);
		   
		   if(Bresp.getErrorCode() == 0)
			  {
			     LOGGER.debug("Enviando Peticion  a Blackston");
			    OfxBankPaymentResponse responseApro = Aproviciona(request, user, Bresp.getIdBitacora()+"");
			    
			    if(responseApro.getBILLPAYMSGSRSV1().getPMTTRNRS().getSTATUS().get(0).getCODE() == 0){
			    	
			    	String date_transaction = DFormat.format(new Date());
			    	ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
			    	/**ENVIAR CORREO DE CONFIRMACION DE PAGO**/
			    	//UtilsMC.emailSend(email, Cadena, wAsunto, NOMBRE, FECHA, AUTBAN, IMPORTE, COMISION, MONTO);
			    	 if(request.getIdioma().equalsIgnoreCase("es"))
			    		 UtilsMC.emailSend(user.geteMail(), mapper.getParameter("@MESSAGE_ACI_PAYMENT_EMAIL_ES"), mapper.getParameter("@ASUNTO_ACI_PAGO_ES"), user.getUsr_nombre()+" " + user.getUsr_apellido(),date_transaction, Bresp.getAuthorization(), currformat.format(request.getMonto()), currformat.format(comision),currformat.format(Mtotal),request.getNombreServicio(), project.getUrl());
			    	 else
			    		 UtilsMC.emailSend(user.geteMail(), mapper.getParameter("@MESSAGE_ACI_PAYMENT_EMAIL_EN"), mapper.getParameter("@ASUNTO_ACI_PAGO_EN"), user.getUsr_nombre()+" " + user.getUsr_apellido(),date_transaction, Bresp.getAuthorization(), currformat.format(request.getMonto()), currformat.format(comision),currformat.format(Mtotal), request.getNombreServicio(), project.getUrl());

			    	/*****************************************/
			    	 
			    	String SuccessfulMessage = GetMessage(request.getIdioma(), Bresp.getAuthorization(), Brequest.getCardNumber(), date_transaction, Mtotal);
			    	
			    	
			    	LOGGER.debug("[Message Response: ]" + SuccessfulMessage);
					Mc.setIdError(0);
					Mc.setMensajeError(SuccessfulMessage);
					//Mc.setMensajeError(responseApro.getBILLPAYMSGSRSV1().getPMTTRNRS().getSTATUS().get(0).getMESSAGE()+"\n"+"Auth: "+Bresp.getAuthorization()+"\n"+ DFormat.format(new Date())+"\n" + currformat.format(Mtotal));
				}else
				{
					Mc.setIdError(98);
					Mc.setMensajeError(responseApro.getBILLPAYMSGSRSV1().getPMTTRNRS().getSTATUS().get(0).getMESSAGE());
				}
			    
			  }else
			  {
					Mc.setIdError(99);
					Mc.setMensajeError("Failed to make payment\n" + Bresp.getMessage());
				  }
		
		}catch(Throwable ex){
			LOGGER.error("Error en pago con datos", ex);
			//ex.printStackTrace();
			Mc.setIdError(99);
			Mc.setMensajeError("Failed to make payment");
		}
		//Mc.setIdError(0);
		//Mc.setMensajeError("");
			return Mc;
	}
	
	public  McResponse paymentNotData(PayRequest request){
		
		McResponse Mc =  new McResponse();
		SimpleDateFormat DFormat =  new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");//new SimpleDateFormat("yyyy/MM/dd"); 
		NumberFormat currformat = NumberFormat.getCurrencyInstance(Locale.US);
		try{
		Proveedor proveedor= mapper.getProveedor("ACI");
		//String ct = mapper.getCt(request.getIdUsuario(), request.getIdTarjeta());
		  Card card = mapper.getMobilecardCard(request.getIdUsuario(), request.getIdTarjeta());
		  double comision =  (request.getMonto()*proveedor.getComision_porcentaje());
		  double Mtotal = request.getMonto() + (request.getMonto()*proveedor.getComision_porcentaje());
		  
		  
		   BlackstoneRequest Brequest = new BlackstoneRequest();
		   Brequest.setConcepto("PAGO ACI: " + request.getNombreServicio());
		   Brequest.setCvv2( UtilsService.getSMS(card.getCt()) );
		   Brequest.setCardNumber(UtilsService.getSMS(card.getNumTarjeta()));
		   Brequest.setExpDate(UtilsService.getSMS(card.getVigencia()).replace("/", ""));
		   Brequest.setIdProducto(request.getIdServicio());
		   Brequest.setIdProveedor(proveedor.getId_proveedor().intValue());
		   Brequest.setIdUsuario(request.getIdUsuario());//  8923233103938l
		   Brequest.setImei(request.getImei());
		   Brequest.setModelo(request.getModelo());
		   Brequest.setMonto(Mtotal);
		   Brequest.setSoftware(request.getSoftware());
		   Brequest.setTipo("M");
		   Brequest.setWkey(request.getWkey());
		   Brequest.setZipCode(request.getCpUsuario());
		   
		   User user =  mapper.getUserData(request.getIdUsuario());
		   String edo = mapper.getEstado(request.getEstadoUsuario());
			user.setId_usuario(request.getIdUsuario());
			user.setUsr_direccion(request.getDireccionUsuario());
	    	user.setUsr_ciudad(request.getCiudadUsuario());
	    	user.setUsr_id_estado(edo);
	    	user.setId_estado(request.getEstadoUsuario());
			user.setUsr_cp(request.getCpUsuario());
			mapper.updateUser(user);
		   
		   BlackstoneResponse Bresp = paymentBlacks.BlackstonePay(Brequest);
		   
		   //guardar en bitacora aci detalle
		   AciDetail details = new AciDetail();
		   details.setId_bitacora(Bresp.getIdBitacora());
		   details.setId_service(request.getIdServicio());
		   details.setComision(comision);
		   details.setCard_number(card.getNumTarjeta());
		   details.setId_user(request.getIdUsuario());
		   details.setLat(request.getLat());
		   details.setLon(request.getLon());
		   details.setAmount(Mtotal);
		   mapper.DetailInsert(details);
		   
		  if(Bresp.getErrorCode() == 0)
		  {
		
			
			OfxBankPaymentResponse responseApro = Aproviciona(request, user, Bresp.getIdBitacora()+"");
			
			if(responseApro.getBILLPAYMSGSRSV1().getPMTTRNRS().getSTATUS().get(0).getCODE() == 0){
				
				String date_transaction = DFormat.format(new Date());
				ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");
				/**ENVIAR CORREO DE CONFIRMACION DE PAGO**/
		    	//UtilsMC.emailSend(email, Cadena, wAsunto, NOMBRE, FECHA, AUTBAN, IMPORTE, COMISION, MONTO);
		    	 if(request.getIdioma().equalsIgnoreCase("es"))
		    		 UtilsMC.emailSend(user.geteMail(), mapper.getParameter("@MESSAGE_ACI_PAYMENT_EMAIL_ES"), mapper.getParameter("@ASUNTO_ACI_PAGO_ES"), user.getUsr_nombre()+" " + user.getUsr_apellido(),date_transaction, Bresp.getAuthorization(), currformat.format(request.getMonto()), currformat.format(comision),currformat.format(Mtotal),request.getNombreServicio(),project.getUrl());
		    	 else
		    		 UtilsMC.emailSend(user.geteMail(), mapper.getParameter("@MESSAGE_ACI_PAYMENT_EMAIL_EN"), mapper.getParameter("@ASUNTO_ACI_PAGO_EN"), user.getUsr_nombre()+" " + user.getUsr_apellido(),date_transaction, Bresp.getAuthorization(), currformat.format(request.getMonto()), currformat.format(comision),currformat.format(Mtotal), request.getNombreServicio(),project.getUrl());

		    	/*****************************************/
		    	 
		    	 String SuccessfulMessage = GetMessage(request.getIdioma(), Bresp.getAuthorization(), Brequest.getCardNumber(), date_transaction, Mtotal);
			    	
			    	
			    	LOGGER.debug("[Message Response: ]" + SuccessfulMessage);
					Mc.setIdError(0);
					Mc.setMensajeError(SuccessfulMessage); 
		    	 
				//Mc.setIdError(0);
				//Mc.setMensajeError(responseApro.getBILLPAYMSGSRSV1().getPMTTRNRS().getSTATUS().get(0).getMESSAGE() +"\n"+"Auth: "+Bresp.getAuthorization()+"\n"+ DFormat.format(new Date())+"\n" + currformat.format(Mtotal));
			}else
			{
				Mc.setIdError(98);
				Mc.setMensajeError(responseApro.getBILLPAYMSGSRSV1().getPMTTRNRS().getSTATUS().get(0).getMESSAGE());
			}
			
			
		  }else
		  {
			Mc.setIdError(99);
			Mc.setMensajeError("Failed to make payment\n" + Bresp.getMessage());
		  }
		
		}catch(Exception ex){
			LOGGER.debug("Error en el pago sin datos", ex);
			Mc.setIdError(99);
			Mc.setMensajeError("Failed to make payment");
		}
		return Mc;
	}
	
	
	private String GetMessage(String idioma, String autorizacion, String card, String fecha_transaccion, double monto){
		String SuccessfulMessage = "";
		NumberFormat currformat = NumberFormat.getCurrencyInstance(Locale.US);
    	if(idioma.equalsIgnoreCase("es"))
    		SuccessfulMessage = mapper.getParameter("@MESSAGE_ACI_PAYMENT_ES");
    	else
    		SuccessfulMessage = mapper.getParameter("@MESSAGE_ACI_PAYMENT_EN");
    	
    	SuccessfulMessage = SuccessfulMessage.replace("@AUTH", autorizacion);
    	SuccessfulMessage = SuccessfulMessage.replace("@CARD",UtilsGeneric.maskCard(card, "*"));
    	SuccessfulMessage = SuccessfulMessage.replace("@DATE",fecha_transaccion);
    	SuccessfulMessage = SuccessfulMessage.replace("@AMOUNT",currformat.format(monto));
    	return SuccessfulMessage;
	}
	
	private OfxBankPaymentResponse Aproviciona(PayRequest request, User user, String id_transaccion){
		try{
			boolean update = false;
			
			OfxBankPaymentRequest BankRequest = new OfxBankPaymentRequest();
			SimpleDateFormat DFormat = new SimpleDateFormat("yyyyMMdd"); 
			
			SignOnRequest sigOnRequest = new SignOnRequest();
			SONRQ sONRQ = new SONRQ();
			sONRQ.setDTCLIENT(DFormat.format(new Date()));  //date.getTime()+""
			AcquirerSignon acquirerSignon = new AcquirerSignon();
			acquirerSignon.setORCCACQUIRERID(Constants.ORCCACQUIRERID);
			acquirerSignon.setORCCLOGINID(Constants.ORCCLOGINID);
			acquirerSignon.setORCCPASSWORD(Constants.ORCCPASSWORD);
			sONRQ.setORCCACQUIRERSIGNON(acquirerSignon);
			sONRQ.setLANGUAGE("EN");
			FinancialInstitution financialInstitution = new FinancialInstitution();
			financialInstitution.setORG(Constants.ORG); //MoblieCardUSA
			financialInstitution.setFID(Constants.FID); //  500170
			sONRQ.setFI(financialInstitution);
			sONRQ.setAPPID(Constants.APPID);
			sONRQ.setAPPVER(Constants.APPVER);
			sigOnRequest.setSONRQ(sONRQ);
		
			
			BankPaymentRequest bankPaymentRequest = new BankPaymentRequest();
			PMTTRNRQ pMTTRNRQ = new PMTTRNRQ();
			pMTTRNRQ.setTRNUID(id_transaccion); //request.getIdTransaction()+""
			PMTRQ pMTRQ = new PMTRQ();
			PMTINFO pMTINFO = new PMTINFO();
			pMTINFO.setTRNAMT(request.getMonto());//320
			Payee payee =  new Payee();
			payee.setNAME(request.getNombreServicio());//"Direct TV"
			payee.setADDR1(request.getDireccionServicio());//"123"
			payee.setADDR2("");//
			
			HashMap<String, String> dataSplit = dataSplit(request.getCiudadServicio());
			
			payee.setCITY((String)dataSplit.get("city")); //"Princeton"
			payee.setSTATE((String)dataSplit.get("state"));//"NJ"
			payee.setPOSTALCODE((String)dataSplit.get("cp")); //"08540"
			
			payee.setCOUNTRY(CountryCode.US);
			pMTINFO.setPAYEE(payee);
			pMTINFO.setPAYACCT(request.getReferencia()); //"x"
			pMTINFO.setDTDUE(DFormat.format(new Date())); //date.getTime()+""
			
			
			
			Payer payer = new Payer();
			
			payer.setORCCPAYERID(request.getIdUsuario()+""); //"x"
			PayerNameType payerNameType = new PayerNameType();
			payerNameType.setORCCFULLNAME(user.getUsr_nombre()+ " " + user.getUsr_apellido()); //"Test user"  
			payer.setORCCPAYERNAME(payerNameType);
			payer.setADDR1(user.getUsr_direccion()); //"111"
			payer.setADDR2("");
			payer.setCITY(user.getUsr_ciudad()); //"Princeton"
			payer.setSTATE(user.getUsr_id_estado()); //"NJ"
			payer.setPOSTALCODE(user.getUsr_cp()); //"08540"
			payer.setCOUNTRY(CountryCode.US);
			pMTINFO.setORCCPAYER(payer);
			pMTINFO.setORCCFIELD1(""); //Extra paymentinformation preguntar
			pMTINFO.setORCCFIELD2(""); //Extra paymentinformation
			
			pMTRQ.setPMTINFO(pMTINFO);
			pMTTRNRQ.setPMTRQ(pMTRQ);
			bankPaymentRequest.setPMTTRNRQ(pMTTRNRQ);
			
			BankRequest.setSIGNONMSGSRQV1(sigOnRequest);
		    BankRequest.setBILLPAYMSGSRQV1(bankPaymentRequest);
		    return SendRequest(BankRequest);
		}catch(Exception ex){
			LOGGER.error("Error al enviar peticion: ", ex);
			return null;
		}
	}
	
	public OfxCancelPaymentResponse cancel(){
		
			OfxCancelPaymentRequest cancel_request = new OfxCancelPaymentRequest();
			SimpleDateFormat DFormat = new SimpleDateFormat("yyyyMMdd"); 
			
			SignOnRequest sigOnRequest = new SignOnRequest();
			SONRQ sONRQ = new SONRQ();
			sONRQ.setDTCLIENT(DFormat.format(new Date()));  //20170324date.getTime()+""
			AcquirerSignon acquirerSignon = new AcquirerSignon();
			acquirerSignon.setORCCACQUIRERID(Constants.ORCCACQUIRERID);
			acquirerSignon.setORCCLOGINID(Constants.ORCCLOGINID);
			acquirerSignon.setORCCPASSWORD(Constants.ORCCPASSWORD);
			sONRQ.setORCCACQUIRERSIGNON(acquirerSignon);
			sONRQ.setLANGUAGE("EN");
			FinancialInstitution financialInstitution = new FinancialInstitution();
			financialInstitution.setORG(Constants.ORG); //MoblieCardUSA
			financialInstitution.setFID(Constants.FID); //  500170
			sONRQ.setFI(financialInstitution);
			sONRQ.setAPPID(Constants.APPID);
			sONRQ.setAPPVER(Constants.APPVER);
			sigOnRequest.setSONRQ(sONRQ);
			
			cancel_request.setSIGNONMSGSRQV1(sigOnRequest);
		
		CancelPaymentRequest cancel_pay = new CancelPaymentRequest();
		//cancel_pay.setORCCORIGINATORPAYMENTID("1223423");
		cancel_pay.setORCCPAYMENTID(12333l); //98037102
		cancel_request.setPMTCANCRQ(cancel_pay);
		
		
		
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		 //EJEMPLO CREAR REQUEST
		map.add("xml",Utils.getXMLRequestCancel(cancel_request,null,OfxCancelPaymentRequest.class).replace("xmlns=\"http://www.princetonecom.com/pa/bankpaymentresponse2\"", "xmlns=\"http://www.princetonecom.com/pa/cancelpaymentrequest2\"").replace("standalone=\"yes\"", ""));//.replace("xmlns=\"http://www.princetonecom.com/pa/bankpaymentresponse2\"", "xmlns=\"http://www.princetonecom.com/pa/bankpaymentrequest2\"").replace("standalone=\"yes\"", ""));
		
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

		ResponseEntity<String> response = restTemplate.postForEntity( endPoint_cancel, request , String.class );
		LOGGER.debug("Request: " + Utils.getXMLRequestCancel(cancel_request,null,OfxBankPaymentRequest.class,SignOnRequest.class,CancelPaymentRequest.class).replace("xmlns=\"http://www.princetonecom.com/pa/cancelpaymentresponse2\"", "xmlns=\"http://www.princetonecom.com/pa/bankpaymentrequest2\"").replace("standalone=\"yes\"", ""));//.replace("xmlns=\"http://www.princetonecom.com/pa/bankpaymentresponse2\"", "xmlns=\"http://www.princetonecom.com/pa/bankpaymentrequest2\"").replace("standalone=\"yes\"", ""));
		LOGGER.debug("Response ACI: " + response.getBody());
		
		OfxCancelPaymentResponse BankResponse;
		BankResponse = Utils.getObjectResponseCancel(response.getBody().replace("xmlns=\"http://www.princetonecom.com/pa/cancelpaymentresponse2\"", "xmlns=\"http://www.princetonecom.com/pa/bankpaymentresponse2\"").replace("standalone=\"yes\"", ""), OfxCancelPaymentResponse.class);
		return BankResponse;
		
	}
	
	
	private OfxBankPaymentResponse SendRequest(OfxBankPaymentRequest BankRequest){
		try{
			LOGGER.debug("Enviando Peticion ACI: ");
			
			RequestFactoryBean rfb = new RequestFactoryBean();
			RestTemplate restTemplate = getRestemplate();//new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			 //EJEMPLO CREAR REQUEST
			
			LOGGER.debug("Request: " + Utils.getXMLRequest(BankRequest,null,OfxBankPaymentRequest.class).replace("xmlns=\"http://www.princetonecom.com/pa/bankpaymentresponse2\"", "xmlns=\"http://www.princetonecom.com/pa/bankpaymentrequest2\"").replace("standalone=\"yes\"", ""));
			
			map.add("xml",Utils.getXMLRequest(BankRequest,null,OfxBankPaymentRequest.class).replace("xmlns=\"http://www.princetonecom.com/pa/bankpaymentresponse2\"", "xmlns=\"http://www.princetonecom.com/pa/bankpaymentrequest2\"").replace("standalone=\"yes\"", ""));
			
			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

			ResponseEntity<String> response = restTemplate.postForEntity( endPoint_Pay, request , String.class );
			LOGGER.debug("Response ACI: " + response.getBody().replaceAll("\\s", ""));
			
			OfxBankPaymentResponse BankResponse;
			BankResponse = Utils.getObjectResponse(response.getBody(), OfxBankPaymentResponse.class);
			
			detailSave(BankResponse, Long.parseLong(BankRequest.getBILLPAYMSGSRQV1().getPMTTRNRQ().getTRNUID()));
			
			LOGGER.debug("Finalizando peticion ACI");
			return BankResponse;
			
		}catch(Exception ex){
			LOGGER.error("ERROR AL ENVIAR REQUEST: " + ex.getMessage());
			return null;
		}
	}
	
	private RestTemplate getRestemplate() {
		
	try{
		
		 KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		    keyStore.load(this.getClass().getResourceAsStream("KeyStore.jks"), "addcel".toCharArray());
		    LOGGER.debug("Key store cargado " + keyStore.size());
		    SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
		            new SSLContextBuilder()
		                    .loadTrustMaterial(null, new TrustSelfSignedStrategy())
		                    .loadKeyMaterial(keyStore, "addcel".toCharArray())
		                    .build(),
		                    ALLOW_ALL_HOSTNAME_VERIFIER);

		    CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();

		    ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
		    RestTemplate restTemplate = new RestTemplate(requestFactory);
		    return restTemplate;
	}catch(Exception ex){
		LOGGER.error("Error al cargar store ", ex);
		return null;
	}
	}
	
	private void detailSave(OfxBankPaymentResponse response , long id_bitacora)
	{
		AciDetail detail = new AciDetail();
		double amount = 0;
		detail.setAmount(amount);
		try{
			
			detail.setId_bitacora(id_bitacora);
			detail.setPmttrnrs_status_code(response.getBILLPAYMSGSRSV1().getPMTTRNRS().getSTATUS().get(0).getCODE());
			detail.setPmttrnrs_status_message(response.getBILLPAYMSGSRSV1().getPMTTRNRS().getSTATUS().get(0).getMESSAGE());
			detail.setPmttrnrs_status_severity(response.getBILLPAYMSGSRSV1().getPMTTRNRS().getSTATUS().get(0).getSEVERITY().value());
			
			
			
			if(response.getBILLPAYMSGSRSV1().getPMTTRNRS().getPMTRS() != null && response.getBILLPAYMSGSRSV1().getPMTTRNRS().getPMTRS().getPMTPRCSTS() != null){
				
				
				detail.setPmtprcsts_dtpmtprc(response.getBILLPAYMSGSRSV1().getPMTTRNRS().getPMTRS().getPMTPRCSTS().getDTPMTPRC());
				detail.setPmtprcsts_orccexpectedpmtdate(response.getBILLPAYMSGSRSV1().getPMTTRNRS().getPMTRS().getPMTPRCSTS().getORCCEXPECTEDPMTDATE());
				detail.setPmtprcsts_orccpaymentid(Long.parseLong(response.getBILLPAYMSGSRSV1().getPMTTRNRS().getPMTRS().getPMTPRCSTS().getORCCPAYMENTID().toString()));
				detail.setPmtprcsts_pmtprccode(response.getBILLPAYMSGSRSV1().getPMTTRNRS().getPMTRS().getPMTPRCSTS().getPMTPRCCODE());
				
				if(response.getBILLPAYMSGSRSV1().getPMTTRNRS().getPMTRS().getPMTINFO() != null){
					try{
					 amount = response.getBILLPAYMSGSRSV1().getPMTTRNRS().getPMTRS().getPMTINFO().getTRNAMT();
					 detail.setAmount(amount);
					}catch(Exception ex){
						LOGGER.error("ERROR al recuperar monto de aprovisionamiento: ", ex);
					}
				}
				
				
			}
			else
			{
				detail.setPmtprcsts_dtpmtprc("");
				detail.setPmtprcsts_orccexpectedpmtdate("");
				detail.setPmtprcsts_orccpaymentid(-1);
				detail.setPmtprcsts_pmtprccode("");
			}
			
			mapper.DetailUpdate(detail);
			
		}catch(Exception ex){
			LOGGER.error("Error al guardar detalle: ", ex);
		}
	}
	
	private HashMap<String, String> dataSplit(String data){
		//NEWARK, NJ 07101
		HashMap<String, String> map = new HashMap<>();
		map.put("city", data.isEmpty() ? "" : data.split(",")[0]);
		map.put("state", data.isEmpty() ? "" : data.split(",")[1].trim().split(" ")[0]);
		map.put("cp", data.isEmpty() ? "" : data.split(",")[1].trim().split(" ")[1]);
		return map;
		
	}

}
