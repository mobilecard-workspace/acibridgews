package com.addcel.aci.bridge.spring.config;

import java.security.KeyStore;
import java.sql.SQLException;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.ibatis.datasource.pooled.PooledDataSource;

import javax.naming.NamingException;
import javax.sql.DataSource;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.addcel.aci.bridge.spring")
@MapperScan("com.addcel.aci.bridge.mybatis.model.mapper")
@PropertySource("classpath:properties/datasource.properties")
public class AppConfig {

	public static final X509HostnameVerifier ALLOW_ALL_HOSTNAME_VERIFIER     = new AllowAllHostnameVerifier();
	
	
	/**
	 * Valores recuperados de datasource.properties
	 */
	@Value( "${jdbc.url}" ) private String jdbcUrl;
    @Value( "${jdbc.driverClassName}" ) private String driverClassName;
    @Value( "${jdbc.username}" ) private String username;
    @Value( "${jdbc.password}" ) private String password;
    
	
     /**
      * Crear Datasource para la conexion
      * @return DataSource
      */
    public DataSource dataSource(){
		JndiObjectFactoryBean dsource = new JndiObjectFactoryBean();
		 dsource.setJndiName("java:/MobilecardDS");
		 dsource.setExpectedType(DataSource.class);
			try {
				dsource.afterPropertiesSet();
			} catch (IllegalArgumentException | NamingException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			return (DataSource) dsource.getObject();
	 }
	 
    /*
    public DataSource dataSource() {
    	org.apache.commons.dbcp.BasicDataSource dataSource;
    }
    
    /*<bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource"  
            destroy-method="close" p:driverClassName="com.mysql.jdbc.Driver"  
            p:url="jdbc:mysql://127.0.0.1:3306/mybatis?characterEncoding=utf8"   
            p:username="root" p:password="password"  
            p:maxActive="10" p:maxIdle="10">  
      </bean>  */
    
	
	  /**
	   *  Crear DataSourceTransactionManager a partir del datasource
	   * @return DataSourceTransactionManager
	   */
	  @Bean
       public DataSourceTransactionManager transactionManager()
	    {
	        return new DataSourceTransactionManager(dataSource());
	    }
	     /**
	      * Crear sessionFactory
	      * @return SqlSessionFactoryBean
	      * @throws Exception
	      */
	    @Bean
	    public SqlSessionFactoryBean sqlSessionFactoryBean() throws Exception{
	        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
	        sessionFactory.setDataSource(dataSource());        
	        // mybatis mapper
	        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/*.xml"));
	        sessionFactory.setTypeAliasesPackage("com.addcel.aci.bridge.mybatis.model.vo");
	        
	        return sessionFactory;
	    }
	    
	    
	  /*  @Bean
	    public RestTemplate restTemplate() {
	        return new RestTemplate(clientHttpRequestFactory());
	    }

	    private ClientHttpRequestFactory clientHttpRequestFactory() {
	    	try{
	    	KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		    keyStore.load(this.getClass().getResourceAsStream("KeyStore.jks"), "addcel".toCharArray());
		  
		    SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
		                    new SSLContextBuilder()
		                    .loadTrustMaterial(null, new TrustSelfSignedStrategy())
		                    .loadKeyMaterial(keyStore, "addcel".toCharArray())
		                    .build(),
		                    ALLOW_ALL_HOSTNAME_VERIFIER);

		    CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();

		    ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
	    	
	    	//	        factory.setReadTimeout(2000);
	        //factory.setConnectTimeout(2000);
	        return requestFactory;
	    	}catch(Exception ex){
	    		return null;
	    	}
	    }*/
	    
}
