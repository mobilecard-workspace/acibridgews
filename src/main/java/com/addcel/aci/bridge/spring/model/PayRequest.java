package com.addcel.aci.bridge.spring.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PayRequest {
	
	private long idUsuario;
	private int idTarjeta;
	private String codigo; //cvv2
	private String direccionUsuario; //direccion 
	private String ciudadUsuario; 
	private int estadoUsuario; 
	private String cpUsuario;
	private double monto;
	private String referencia;
	
	private String nombreServicio;
	private String direccionServicio;
	private String ciudadServicio;
	private int idServicio;
	
	private String software;
	private String wkey;
	private String imei;
	private String modelo;
	
	private String idioma;
	
	private double lat;
	private double lon;
	
	public PayRequest() {
		// TODO Auto-generated constructor stub
	}

	public double getLat() {
		return lat;
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	public double getLon() {
		return lon;
	}
	
	public void setLon(double lon) {
		this.lon = lon;
	}
	
	public String getIdioma() {
		return idioma;
	}
	
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	
	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getIdTarjeta() {
		return idTarjeta;
	}

	public void setIdTarjeta(int idTarjeta) {
		this.idTarjeta = idTarjeta;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDireccionUsuario() {
		return direccionUsuario;
	}

	public void setDireccionUsuario(String direccionUsuario) {
		this.direccionUsuario = direccionUsuario;
	}

	public String getCiudadUsuario() {
		return ciudadUsuario;
	}

	public void setCiudadUsuario(String ciudadUsuario) {
		this.ciudadUsuario = ciudadUsuario;
	}

	public int getEstadoUsuario() {
		return estadoUsuario;
	}

	public void setEstadoUsuario(int estadoUsuario) {
		this.estadoUsuario = estadoUsuario;
	}

	public String getCpUsuario() {
		return cpUsuario;
	}

	public void setCpUsuario(String cpUsuario) {
		this.cpUsuario = cpUsuario;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getNombreServicio() {
		return nombreServicio;
	}

	public void setNombreServicio(String nombreServicio) {
		this.nombreServicio = nombreServicio;
	}

	public String getDireccionServicio() {
		return direccionServicio;
	}

	public void setDireccionServicio(String direccionServicio) {
		this.direccionServicio = direccionServicio;
	}

	public String getCiudadServicio() {
		return ciudadServicio;
	}

	public void setCiudadServicio(String ciudadServicio) {
		this.ciudadServicio = ciudadServicio;
	}

	public int getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getWkey() {
		return wkey;
	}

	public void setWkey(String wkey) {
		this.wkey = wkey;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
    public String getReferencia() {
		return referencia;
	}
    
    public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	
	//{ "idUsuario": 8923233103938, "concepto": "Test Blackstone", "idProveedor": 0, "idProducto": 0, "monto": 10.0, "cvv2": "123", "software": "Web", "wkey": "123", "imei": "12345678", "tipo": "M", "modelo": "IOS", "zipCode": "03301" }


}
