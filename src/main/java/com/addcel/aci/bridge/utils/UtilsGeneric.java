package com.addcel.aci.bridge.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.addcel.aci.bridge.client.payment.vo.OfxBankPaymentRequest;
import com.addcel.aci.bridge.client.payment.vo.OfxBankPaymentResponse;

@Component
public class UtilsGeneric {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UtilsGeneric.class);
	
	@Autowired
	private ObjectMapper mapperJk;
	
	public <T> String objectToJson(T object){
		String json=null;
		try {
			json=mapperJk.writeValueAsString(object);
		} catch (JsonGenerationException e) {
			LOGGER.error("1.- Error al generar json: {}",e);
		} catch (JsonMappingException e) {
			LOGGER.error("2.- Error al mapperar objeccto: {}",e);
		} catch (IOException e) {
			LOGGER.error("3.- Error al generar json: {}",e);
		}
		return json;
	}

	public <T> Object jsonToObject(String json, Class<T> clase) {		
		Object obj = null;
		try {
			obj = mapperJk.readValue(json, clase);
		} catch (JsonParseException e) {
			LOGGER.error("Error al parsear cadena json 1: {}", e);
		} catch (JsonMappingException e) {
			LOGGER.error("Error al parsear cadena json 2: {}", e);
		} catch (IOException e) {
			LOGGER.error("Error al parsear cadena json 3: {}", e);
		}
		return obj;
	}
	
	public static boolean isNullorEmpty(String cadena){
		
		if(cadena == null || cadena.isEmpty())
			return true;
		else
			return false;
	}
	
	 
	  public static String maskCard(String card, String placeholder) {
	    String result = "";
	    LOGGER.debug("Longitud tarjeta: {}", card.length());
	    for (int i = 0; i < card.length(); i++) {
	      if (i >= 4 && i <= 11) {
	        if ((i + 1) % 4 == 0) {
	          result += (placeholder + " ");
	        } else {
	          result += placeholder;
	        }
	      } else {
	        if ((i + 1) % 4 == 0) {
	          result += (card.charAt(i) + " ");
	        } else {
	          result += card.charAt(i);
	        }
	      }
	    }
	    return result;
	  }

}
