package com.addcel.aci.bridge.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import crypto.Crypto;


public class UtilsMC {
	
	 private static final Logger LOGGER = LoggerFactory.getLogger(UtilsMC.class);
	 private static final String TelefonoServicio = "5525963513";
	
	 public static String setSMS(String Telefono) {
	        return Crypto.aesEncrypt(parsePass(TelefonoServicio), Telefono);
	    }

	    public static String getSMS(String Telefono) {
	        return Crypto.aesDecrypt(parsePass(TelefonoServicio), Telefono);
	    }

	    public static String parsePass(String pass) {
	        int len = pass.length();
	        String key = "";

	        for (int i = 0; i < 32 / len; i++) {
	            key += pass;
	        }

	        int carry = 0;
	        while (key.length() < 32) {
	            key += pass.charAt(carry);
	            carry++;
	        }
	        return key;
	    }
	    
	    public static String generaSemillaAux(String pass) {
	        int len = pass.length();
	        String key = "";

	        int carry;
	        for (carry = 0; carry < 32 / len; ++carry) {
	          key = key + pass;
	        }

	        for (carry = 0; key.length() < 32; ++carry) {
	          key = key + pass.charAt(carry);
	        }

	        return key;
	      }
	    
	  
	    
	    public static void emailSend(String email, String Cadena, String wAsunto, String NOMBRE, String FECHA, String AUTBAN, String IMPORTE, String COMISION, String MONTO, String PRODUCTO, String urlMailsender ){
	   	 
	   	 // if (idi.equals("es")) {
	             if (patternMatches(email, ".*@(hotmail|live|msn|outlook)\\..*")) {
	               LOGGER.info("[ENVIANDO CORREO MICROSOFT]...");
	               SendMailMicrosoft(email, Cadena , wAsunto,  NOMBRE,  FECHA,  AUTBAN,  IMPORTE,  COMISION,  MONTO, PRODUCTO,urlMailsender);
	             } else {
	               LOGGER.info("entro else ");
	               /*SendMailNormal(email, MessageType, subject, usuario,
	                   password);*/
	               SendMailMicrosoft(email, Cadena , wAsunto, NOMBRE,  FECHA,  AUTBAN,  IMPORTE,  COMISION,  MONTO, PRODUCTO,urlMailsender);
	             }
	             LOGGER.info("email enviado correctamente " + email);
	    
	     }
	    
	    public static boolean SendMailMicrosoft(String sMail, String Cadena, String wAsunto,String NOMBRE, String FECHA, String AUTBAN, String IMPORTE, String COMISION, String MONTO, String PRODUCTO, String urlMailsender) {
	    	    boolean flag = false;
	    	   
	    	    try {

	    	      Cadena = Cadena.replace("#NOMBRE#", NOMBRE);
	    	      Cadena = Cadena.replace("#FECHA#", FECHA);
	    	      Cadena = Cadena.replace("#AUTBAN#", AUTBAN);
	    	      Cadena = Cadena.replace("#IMPORTE#", IMPORTE);
	    	      Cadena = Cadena.replace("#COMISION#", COMISION);
	    	      Cadena = Cadena.replace("#MONTO#", MONTO);
	    	      Cadena = Cadena.replace("#PRODUCTO#",PRODUCTO);
	    	      
	    	      MailSender correo = new MailSender();
	    	      String from = "no-reply@addcel.com";
	    	      String[] to = {sMail};
	    	      correo.setCc(new String[] {});
	    	      correo.setBody(Cadena);
	    	      correo.setFrom(from);
	    	      correo.setSubject(wAsunto);
	    	      correo.setTo(to);
	    	      
	    	      Gson gson = new Gson();
	    	      String json = gson.toJson(correo);
	    	      LOGGER.info("antes de enviar mail");
	    	      MailSender(json);
	    	     // enviarCorreoMicrosoft(correo);
	    	      LOGGER.info("despues de enviar mail");
	    	      LOGGER.debug("mensaje hotmail enviado : " + sMail);
	    	    } catch (Exception ex) {
	    	      LOGGER.error("Ocurrio un error al enviar email hotmail {}:  {}", sMail, ex);
	    	    }
	    	    return flag;
	    	  }
	    
	    public static void MailSender(String data) {
    		String line = null;
    		StringBuilder sb = new StringBuilder();
    		try {
    			LOGGER.info("Iniciando proceso de envio email Microsoft. ");
    			URL url = new URL(Constants.urlSendmail);
    			LOGGER.info("Conectando con " + Constants.urlSendmail);
    			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

    			urlConnection.setDoOutput(true);
    			urlConnection.setRequestProperty("Content-Type", "application/json");
    			urlConnection.setRequestProperty("Accept", "application/json");
    			urlConnection.setRequestMethod("POST");

    			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
    			writter.write(data);
    			writter.flush();

    			LOGGER.info("Datos enviados, esperando respuesta");

    			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

    			while ((line = reader.readLine()) != null) {
    				sb.append(line);
    			}

    			LOGGER.info("Respuesta del servidor(envio email Microsoft) " + sb.toString());
    		} catch (Exception ex) {
    			LOGGER.error("Error en: sendMail, al enviar el email ", ex);
    		}
    	}

	    
	    public static boolean patternMatches(String email, String pattern){
	    	boolean flag = false;
	    	
	    	Pattern pat = Pattern.compile(pattern); //".*@(hotmail|live|msn|outlook)\\..*"
		     Matcher mat = pat.matcher(email.toLowerCase());
		     if (mat.matches()) {
		    	 flag = true;
		     }
	    	return flag;
	    }
	
	 public static Object validarCampos(Object pObjeto) {
	        Object objeto = pObjeto;
	        Method metodos[] = pObjeto.getClass().getMethods();
	        for (int i = 0; i < metodos.length; i++) {
	            Method metodo = metodos[i];
	            //Si es un metodo get o is lo utilizo con su equivalente set
	            if ((metodo.getName().substring(0, 3).equalsIgnoreCase("get") || metodo.getName().substring(0, 2).equalsIgnoreCase("is")) && !metodo.getName().equals("getClass")) {
	                String methodNameSet = "";
	                if(metodo.getName().substring(0, 3).equalsIgnoreCase("get")){
	                    methodNameSet = metodo.getName().replaceFirst("get", "set");
	                }else{
	                    methodNameSet = methodNameSet.replaceFirst("is", "set");
	                }
	                try {
	                    Method metodoSet = pObjeto.getClass().getMethod(methodNameSet, metodo.getReturnType());
	                    if (metodoSet != null) {
	                        //Datos numericos
	                        //Si es byte
	                        if (metodo.getReturnType().equals(java.lang.Byte.class)) {
	                            Byte valor = (Byte)metodo.invoke(pObjeto, new Object[0]);
	                            if(valor==null){
	                                metodoSet.invoke(pObjeto, 0);
	                            }
	                        }
	                        //Si es bigDecimal
	                        if (metodo.getReturnType().equals(java.math.BigDecimal.class)) {
	                            BigDecimal valor = (BigDecimal)metodo.invoke(pObjeto, new Object[0]);
	                            if(valor==null){
	                                metodoSet.invoke(pObjeto, new BigDecimal(0));
	                            }
	                        }
	                        // Si es Double
	                        if (metodo.getReturnType().equals(java.lang.Double.class)) {
	                            Double valor = (Double)metodo.invoke(pObjeto, new Object[0]);
	                            if(valor==null){
	                                metodoSet.invoke(pObjeto, new Double(0));
	                            }
	                        }
	                        //si es Integer
	                        if (metodo.getReturnType().equals(java.lang.Integer.class)) {
	                            Integer valor = (Integer)metodo.invoke(pObjeto, new Object[0]);
	                            if(valor==null){
	                                metodoSet.invoke(pObjeto, new Integer(0));
	                            }
	                        }
	                        //Si es un string
	                        if (metodo.getReturnType().equals(java.lang.String.class)) {
	                            String valor = (String)metodo.invoke(pObjeto, new Object[0]);
	                            if(valor==null){
	                                metodoSet.invoke(pObjeto, "");
	                            }
	                        }
	                        //Si es una lista
	                        if (metodo.getReturnType().equals(java.util.List.class)) {
	                            List objetosList = (List)metodo.invoke(pObjeto, new Object[0]);
	                            for(Object objetoFromList:objetosList){
	                            	UtilsMC.validarCampos(objetoFromList);
	                            }
	                        }
	                        //Si es date
	                        if (metodo.getReturnType().equals(java.util.Date.class)) {
	                            Date valor = (Date)metodo.invoke(pObjeto, new Object[0]);
	                            if(valor==null){
	                                metodoSet.invoke(pObjeto, new Date());
	                            }
	                        }
	                        //Si es primitivo
	                        if (metodo.getReturnType().isPrimitive()) {
	                            //los primitivos no permiten null
	                        }
	                    }
	                } catch (Exception e) {
	                }
	            }
	        }
	        return objeto;
	    }
	 
	 /**
	   * Tarjeta que sustituye los digitos correspondientes a los indices 4 - 11 con un caracter
	   * cualquiera.
	   * Si un caracter en cuestion se encuentra antes de un indice multiplo de 4 agrega un espacio a
	   * la
	   * representacion
	   *
	   * @param card - El numero de tarjeta a enmascarar
	   * @param placeholder - Caracter a utilizar como sustitucion
	   * @return String representando la tarjeta enmascarada
	   */
	  public static String maskCard(String card, String placeholder) {
	    String result = "";
	    LOGGER.debug("Longitud tarjeta: {}", card.length());
	    for (int i = 0; i < card.length(); i++) {
	      if (i >= 4 && i <= 11) {
	        if ((i + 1) % 4 == 0) {
	          result += (placeholder + " ");
	        } else {
	          result += placeholder;
	        }
	      } else {
	        if ((i + 1) % 4 == 0) {
	          result += (card.charAt(i) + " ");
	        } else {
	          result += card.charAt(i);
	        }
	      }
	    }
	    return result;
	  }
	  
	  public static String formatCard(String card) {

		  String result = "";
		  for (int i = 0; i < card.length(); i++) {
		  if ((i + 1) % 4 == 0) {
		  result += (card.charAt(i) + " ");
		  } else {
		  result += card.charAt(i);
		  }
		  }
		  return result;
		  }
	  
	  private static final String BASE = "0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		
		public static String generatePassword(int longitud){
			StringBuffer contrasena = new StringBuffer();
			int numero = 0;
			for(int i = 0; i < longitud; i++){ //1
			    numero = (int)(Math.random()*(BASE.length()-1)); //2
			    contrasena.append(BASE.substring(numero, numero+1)); //4
			}
			return contrasena.toString();
		}
		
		

}
